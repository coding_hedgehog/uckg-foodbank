from flask import (Flask, render_template, url_for, request, flash, redirect,
                    session, send_from_directory, escape)
from flask_login import LoginManager, login_user, logout_user, login_required
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Message, Mail
from flask_bcrypt import Bcrypt
from sqlalchemy.exc import IntegrityError
from werkzeug.utils import secure_filename
from PIL import Image
import os, random

app = Flask(__name__, static_url_path='/static')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://uckgchurch:uckgpassword@localhost/uckg?auth_plugin=mysql_native_password'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://markalexa:uckgroot@markalexa.mysql.pythonanywhere-services.com/markalexa$uckg'
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
app.config['MAIL_DEBUG'] = True
app.config['MAIL_USERNAME'] = 'foodbankuckg@gmail.com'
app.config['MAIL_PASSWORD'] = ''
app.config['MAIL_DEFAULT_SENDER'] = ''
app.config['MAIL_SUPPRESS_SEND'] = False
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = os.urandom(24)
app.config['UPLOAD_FOLDER'] = os.getcwd()+'/items'             # DEVELOPMENT
#app.config['UPLOAD_FOLDER'] = os.getcwd()+'/foodbank/items'   # PRODUCTION

db = SQLAlchemy(app)
bc = Bcrypt(app)
login_mgr = LoginManager(app)
login_mgr.login_view = 'login'
login_mgr.login_message = 'Something went wrong and we had to log you out for security reasons. Please log in again.'
login_mgr.login_message_category = 'warning'
mail = Mail(app)

FOODS_DIR = os.getcwd()+'/items/foods'
TOILETRIES_DIR = os.getcwd()+'/items/toiletries'
DB_INITIALIZED = False
ALLOWED_EXTENSIONS = {'png', 'jpeg', 'jpg'}
CAPTCHA_IMGS = {index: img for index, img in enumerate(['eighty.png', 'eleven.png',
                'four.png', 'four2.png', 'hundred.png', 'ninety_nine.png',
                'two.png'], start=1)}
RESULTS = {1: 80, 2: 11, 3: 4, 4: 4, 5: 100, 6: 99, 7: 2}

class Food(db.Model):
    __tablename__ = 'groceries'
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    item          = db.Column(db.String(128), nullable=False)
    qty           = db.Column(db.Integer, nullable=False)
    owner_id      = db.Column(db.Integer, db.ForeignKey('branches.id'))

    def __init__(self, item, qty, owner_id):
        self.item = item
        self.qty = qty
        self.owner_id = owner_id

class Toiletry(db.Model):
    __tablename__ = 'toiletries'
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    item          = db.Column(db.String(128), nullable=False)
    qty           = db.Column(db.Integer, nullable=False)
    owner_id      = db.Column(db.Integer, db.ForeignKey('branches.id'))

    def __init__(self, item, qty, owner_id):
        self.item = item
        self.qty = qty
        self.owner_id = owner_id

class Branch(db.Model):
    __tablename__ = 'branches'
    id            = db.Column(db.Integer, primary_key=True, autoincrement=True)
    location      = db.Column(db.String(128), nullable=False, unique=True)
    displayed     = db.Column(db.String(128), nullable=False, unique=True)
    email         = db.Column(db.String(128), nullable=False, unique=True)
    password      = db.Column(db.String(255), nullable=False)
    authenticated = db.Column(db.Boolean, default=False)
    active        = db.Column(db.Boolean, default=False)
    foods         = db.relationship('Food', backref='owner')
    toiletries    = db.relationship('Toiletry', backref='owner')

    def __init__(self, location, displayed, email, password):
        self.location = location
        self.displayed = displayed
        self.email = email
        self.password = bc.generate_password_hash(password).decode()

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

def manipulate_image(img):
    to_resize = Image.open(img)
    width, height = to_resize.size
    resized = to_resize.resize((width//2, height//2))
    image = resized.rotate(-90)
    return image

@login_mgr.user_loader
def load_user(user_id):
    return Branch.query.get(int(user_id))

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/foodbank')
@app.route('/foodbank/')
def reroute():
    return redirect(url_for('index'))

@app.route('/')
def index():
    try: # THIS TRY-EXCEPT HANDLER IS TO HANDLE OCCASIONAL ERROR THROWN BY MYSQL NOT BEING READY (PRODUCTION)
        try:
            global DB_INITIALIZED
            if len(session) != 0:
                return redirect(url_for('interface', location=session['_user_id']))
        except KeyError:
            pass
        if not DB_INITIALIZED:
            db.create_all()
            DB_INITIALIZED = True
        branches = Branch.query.order_by(Branch.displayed.asc()).all()
        approved_branches = [branch for branch in branches if branch.is_authenticated()]
        return render_template('index.html', branches=approved_branches, title='Welcome')
    except:
        return redirect(url_for('index'))

@app.route('/foodbank/<location>')
def foodbank(location):
    branch = Branch.query.filter_by(location=location).first()
    foods = Food.query.filter(Food.owner.has(Branch.location==location)).all()
    toiletries = Toiletry.query.filter(Toiletry.owner.has(Branch.location==location)).all()
    return render_template('interface.html', location=branch, foods=foods,
                            toiletries=toiletries, title=branch.displayed)

@app.route('/church/approve_branch/<location_id>')
def approve_branch(location_id):
    try:
        branch = Branch.query.filter_by(id=int(location_id)).first()
        print(branch.location)
        if not branch.is_authenticated():
            branch.authenticated = True
            db.session.commit()
            os.mkdir(app.config['UPLOAD_FOLDER']+f'/foods/{branch.location}')
            os.mkdir(app.config['UPLOAD_FOLDER']+f'/toiletries/{branch.location}')
            return f'Branch {branch.displayed} has been approved. Thank you, you can now close the tab.', 200
        else:
            return 'This branch has been already approved.', 400
    except:
        return 'Invalid Request', 400

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        edited_location = ''
        location = escape(request.form.get('location'))
        password = request.form.get('password')
        location = location.lower()
        if ' ' in location:
            for char in location:
                if char != ' ':
                    edited_location += char
                else:
                    edited_location += '_'
            location = edited_location
        branch = Branch.query.filter_by(location=location).first()
        if branch and bc.check_password_hash(branch.password, password):
            if branch.is_authenticated():
                print('logging in ....')
                return redirect(url_for('logging_in', location=branch.id))
            else:
                flash('Your church hasn\'t been approved yet. Try again later.', 'info')
        else:
            flash('Login Failed ! Check your logins and try again.', 'danger')
    return render_template('login.html', title='Login')

@app.route('/auth/logging_branch_in/<location>')
def logging_in(location):
    branch_to_login = Branch.query.filter_by(id=int(location)).first()
    branch_to_login.active = True
    db.session.commit()
    login_user(branch_to_login)
    return redirect(url_for('interface', location=branch_to_login.location))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        two_words_location, displayed = None, None
        location = escape(request.form.get('location'))
        email = escape(request.form.get('email'))
        password = request.form.get('password')
        picked_num = request.form.get('picked_num')
        num_given = request.form.get('given_result')
        if int(num_given) == RESULTS[int(picked_num)]:
            if location and password:
                location = location.lower()
                if ' ' in location:
                    two_words_location = ''
                    for char in location:
                        if char != ' ':
                            two_words_location += char
                        else:
                            two_words_location += '_'
                if two_words_location is not None:
                    check_first = Branch.query.filter_by(location=two_words_location.lower()).first()
                else:
                    check_first = Branch.query.filter_by(location=location.lower()).first()
                if not check_first:
                    try:
                        if two_words_location is not None:
                            displayed = ''
                            for char in two_words_location:
                                if char != '_':
                                    displayed += char
                                else:
                                    displayed += ' '
                            split = displayed.split(' ')
                            titled = [el.title() for el in split]
                            to_display = ' '.join(titled)
                            new_foodbank = Branch(location=two_words_location,
                            displayed=to_display, email=email, password=password)
                        else:
                            new_foodbank = Branch(location=location,
                            displayed=location.title(), email=email, password=password)
                        db.session.add(new_foodbank)
                        db.session.commit()
                        if two_words_location is not None:
                            msg = f'Hello,\n\nA representative from the church in {to_display} with email {email}\n\nhas sent you a request to allow them to join UCKG Food Bank site.\nIf you wish to approve this request please click on the link below\notherwise simply ignore this email.\n\nhttp://localhost:5000/church/approve_branch/{new_foodbank.id}\n\nHave a blessed day,\n\nUCKG Foodbank'
                        else:
                            msg = f'Hello,\n\nA representative from the church in {location.title()} with email {email}\n\nhas sent you a request to allow them to join UCKG Food Bank site.\nIf you wish to approve this request please click on the link below\notherwise simply ignore this email.\n\nhttp://localhost:5000/church/approve_branch/{new_foodbank.id}\n\nHave a blessed day,\n\nUCKG Foodbank'
                        msg_to_send = Message(body=msg, subject='Registration Request',
                         sender=email, recipients=[])
                        mail.send(msg_to_send)
                        flash('Request Sent !', 'primary')
                    except IntegrityError:
                        db.session.rollback()
                        flash('That email is already registered with another branch.', 'danger')
                else:
                    flash('Your foodbank is already registered. Log in instead.', 'warning')

        else:
            flash('Incorrect captcha. Please try again.', 'danger')
        return redirect(url_for('register'))
    pick_a_num = random.randint(1, 7)
    pick_a_captcha = CAPTCHA_IMGS[pick_a_num]
    # print('RESULT IS:', RESULTS[pick_a_num])
    src_to_parse = url_for('static', filename=f'images/captcha/{pick_a_captcha}')
    return render_template('register.html', title='Register', captcha=src_to_parse,
                            pick_a_num=pick_a_num)

@app.route('/foodbank/<location>/add_item', methods=['GET', 'POST'])
@login_required
def add_item(location):
    if request.method == 'POST':
        branch = Branch.query.filter_by(location=location).first()
        food_or_toiletry = request.form.get('food_or_toiletry')
        if food_or_toiletry == 'category':
            flash('You need to choose food or toiletry.', 'warning')
            return redirect(url_for('add_item', location=location))
        image = request.files.get('photo')
        qty = int(request.form.get('qty'))
        if image.filename == '':
            flash('No image selected.', 'warning')
            return redirect(url_for('add_item', location=location))
        if image and allowed_file(image.filename):
            filename = secure_filename(image.filename)
            if food_or_toiletry == 'food':
                new_food = Food(item=filename, qty=qty, owner_id=branch.id)
                db.session.add(new_food)
                db.session.commit()
                image = manipulate_image(image)
                image.save(os.path.join(app.config['UPLOAD_FOLDER']+f'/foods/{location}', filename))
                flash('Item Added !', 'primary')
                return redirect(url_for('interface', location=location))
            else:
                new_toiletry = Toiletry(item=filename, qty=qty, owner_id=branch.id)
                db.session.add(new_toiletry)
                db.session.commit()
                image = manipulate_image(image)
                image.save(os.path.join(app.config['UPLOAD_FOLDER']+f'/toiletries/{location}', filename))
                flash('Item Added !', 'primary')
                return redirect(url_for('interface', location=location))
        else:
            flash('Only images are allowed.', 'danger')
    location = Branch.query.filter_by(location=location).first()
    return render_template('add_item.html', location=location, title='Add Item')

@app.route('/foodbank/<category>/<location>/<photo_file>')
def fetch_image(category, location, photo_file):
    target = app.config['UPLOAD_FOLDER']+f'/{category}/{location}'
    return send_from_directory(f'{target}/', photo_file, as_attachment=True)

@app.route('/foodbank/auth/<location>')
@login_required
def interface(location):
    try:
        loc = int(location)
        location = Branch.query.filter_by(id=loc).first()
    except ValueError:
        location = Branch.query.filter_by(location=location).first()
    foods = Food.query.filter(Food.owner.has(Branch.id == location.id)).all()
    toiletries = Toiletry.query.filter(Toiletry.owner.has(Branch.id == location.id)).all()
    return render_template('interface.html', foods=foods, toiletries=toiletries,
                        location=location, title=location.displayed)

@app.route('/foodbank/delete/<category>/<location>/<delete_item>')
@login_required
def delete_item(category, location, delete_item):
    if category == 'foods':
        item_to_delete = Food.query.filter(Food.owner.has(Branch.location == location)).filter_by(item=delete_item).first()
    else:
        item_to_delete = Toiletry.query.filter(Toiletry.owner.has(Branch.location == location)).filter_by(item=delete_item).first()
    db.session.delete(item_to_delete)
    db.session.commit()
    os.remove(app.config['UPLOAD_FOLDER']+f'/{category}/{location}/{delete_item}')
    flash('Item Deleted !', 'warning')
    return redirect(url_for('interface', location=location))

@app.route('/foodbank/change/<category>/<location>/<change_item>/<qty>', methods=['POST'])
@login_required
def change_item(category, location, change_item, qty):
    if category == 'foods':
        item_to_change = Food.query.filter(Food.owner.has(Branch.location == location)).filter_by(item=change_item).first()
    else:
        item_to_change = Toiletry.query.filter(Toiletry.owner.has(Branch.location == location)).filter_by(item=change_item).first()
    if int(qty) == 0:
        db.session.delete(item_to_change)
        db.session.commit()
        os.remove(app.config['UPLOAD_FOLDER']+f'/{category}/{location}/{change_item}')
        flash('Item Deleted !', 'warning')
    else:
        item_to_change.qty = int(qty)
        db.session.commit()
        flash('Quantity Changed !', 'primary')
    return redirect(url_for('interface', location=location))

@app.route('/foodbank/logout/<location>')
@login_required
def logout(location):
    branch_to_logout = Branch.query.filter_by(location=location).first()
    branch_to_logout.active = False
    db.session.commit()
    logout_user()
    flash('Your foodbank is now logged out. Have a blessed day :)', 'primary')
    return redirect(url_for('login'))
